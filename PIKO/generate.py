import time
time.sleep(0.1) 
from machine import Pin, Timer
import random
import json

led = Pin('LED', Pin.OUT)
timer = Timer()
def generate_random(timer):
    random_value = random.uniform(0, 30.0)
    if random_value:  
        led.toggle()
        
    print(json.dumps({"value": random_value}))


timer.init(freq=0.05, mode=Timer.PERIODIC, callback=generate_random)


while True:
    pass  