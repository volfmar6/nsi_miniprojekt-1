jQuery.noConflict();
jQuery(document).ready(function($) {

  //ADD measure form

    
    $('#add-measure-form').submit(function(event) {
        event.preventDefault();

        var timestamp = $('#timestamp').val();
        var temp = $('#temperature').val();

        $.ajax({
            url: '/api',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ timestamp: timestamp, temp: temp }),
            success: function(response) {
            console.log('Measurement created successfully:', response);
                $('#add-measure-form')[0].reset();
                location.reload();
            }
        });
    
    });

    //DELETE measure form 
    $('#delete-measure-form').submit(function(event) {
        event.preventDefault();

        var deleteCount = parseInt($('#deleteCount').val()); //dostanu z inputu value

        $.ajax({
            url: '/api/deleteRange', //zavolám api routu pro deleteCount
            type: 'DELETE',
            data: JSON.stringify({ deleteCount: deleteCount }),
            contentType: 'application/json',
            success: function(response) {
                console.log('Measurement deleted successfully:', response);
                $('#delete-measure-form')[0].reset();
                location.reload(); // Refresh po smazání
            },
            error: function(xhr, status, error) {
                console.error('Error deleting range:', error);
            }

        });
    });

    
    //filter measure form

    $('#filter-measure-form').submit(function(event) {
        event.preventDefault();

        var lastMeasuresCount = parseInt($('#filterCount').val());
        
        //dostanu max ID IDC
        $.ajax({
            url: '/api/maxid',
            type: 'GET',
            success: function(response) {
                // získám požadovaných X záznamů
                $('#measuresContainer').empty();
                
                for (var i = response; i > Math.max(0, response - lastMeasuresCount); i--) {
                    fetchMeasure(i);
                }
            },
            error: function(xhr, status, error) {
                console.error('Error retrieving max ID:', error);
            }
        });
        
    });
    
    function fetchMeasure(measureId) {
        $.ajax({
            url: '/api/' + measureId,
            type: 'GET',
            success: function(response) {
                console.log('dostal jsem data od api_route:',response )
                displayMeasure(response); //zobrazení filtrovaných dat po jednom
                
            },
            error: function(error) {
                console.error('Error retrieving measure:', error);
            }
        });
    }

    function displayMeasure(measure) {
        var measureHtml = 
        '<tr id="measure-' + measure[0]+ '">' +
            '<td>' + measure[0]+'</td>' +
            '<td>' + measure[1] + '</td>' +
            '<td>' + measure[2] + '</td>' +
        '</tr>';

        $('#measuresContainer').append(measureHtml); //zobrazím řádek v tabulce 
    }
    

    $('#register-form').submit(function(event) {
        event.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();
        var confirmpassword = $('#CPassword').val();
        if(password==confirmpassword)
        {
            var errorMessage = ''
            var succMessage= 'Congratulations on your registration!'
            $('#error-label').text(errorMessage); //vymažu error message
            $('#success-label').text(succMessage); //zobrazím success message

            $.ajax({
                url: '/api/register',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({ email: email, password: password }),
                success: function(response) {
                console.log('User created successfully:', response);
                    $('#register-form')[0].reset();
                    window.location.href = 'login';  //přesměrování na login page 
                }
            });
        
        }
        else
        {
            var errorMessage = 'Zadaná hesla se neshodují'
            var succMessage= ''
            $('#error-label').text(errorMessage); //zobrazím error message
            $('#success-label').text(succMessage); //zobrazím success message
        }
        
    });

    $('#login-form').submit(function(event) {
        event.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();

        $.ajax({
            url: '/api/login',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ email: email, password: password }),
            success: function(response) {
            console.log('User logon successfully:', response);
                $('#login-form')[0].reset();
                localStorage.setItem('username', email);
                window.location.href = '/dashboard';
            }
        });     
    });
    $(document).ready(function() {                              //fix na mizení hodnot ... incializace full reloadu nechá zmizet hodnoty
        var storedUsername = localStorage.getItem('username');
        if (storedUsername) {
            $('#username_nav').text('Welcome back, ' + storedUsername + '!');
            $('#logout-button').show();
        }
        else
        {
            
        }
    });

    $('#logout-button').click(function() {  //signout

        localStorage.removeItem('username');
        $('#username-display').text('');
        $('#logout-button').hide();
        location.reload();
    });
});

